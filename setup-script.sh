#!/bin/bash

## ADD SSH KEY TO GITLAB FIRST

## chmod a+x setup-script.sh

sudo apt-get update

## update git
echo " 🖊️ 🖊️ 🖊️ 🖊️  🖊️ 🖊️ 🖊️ 🖊️  🖊️ 🖊️ 🖊️ 🖊️  🖊️ 🖊️ 🖊️ 🖊️ "
echo "--------------- UPDATING GIT ---------------"
echo " 🖊️ 🖊️ 🖊️ 🖊️  🖊️ 🖊️ 🖊️ 🖊️  🖊️ 🖊️ 🖊️ 🖊️  🖊️ 🖊️ 🖊️ 🖊️ "

sudo apt-get install git -y

## update emacs
echo
echo "🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 "
echo "--------------- UPDATING EMACS -----------------------"
echo "🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 🌳 "
echo

sudo apt-get install emacs -y

## install Erlang and Elixir
echo "⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ " 
echo "⚗️------ INSTALL ERLANG AND ELIXIR --------⚗️ "
echo "⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ ⚗️ " 

wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb

sudo apt-get install esl-erlang -y

sudo apt-get install elixir -y

rm ~/erlang-solutions_1.0_all.deb


## Make it pretty

sudo apt-get install gnome-tweak-tool -y

## Install Tela icons

git clone https://github.com/vinceliuice/Tela-icon-theme
mkdir ~/.icons
./Tela-icon-theme -d ~/.icons

### Remove unused pacakages
sudo apt-get autoremove -y

### Install Fira Symbols for Emacs
wget https://github.com/tonsky/FiraCode/files/412440/FiraCode-Regular-Symbol.zip
unzip ~/FiraCode-Regular-Symbol.zip
sudo mv ~/FiraCode-Regular-Symbol.zip ~/.fonts
sudo fc-cache -f -v

#########
# FISH #
########

echo "🐠🐠🐠🐠🐠🐠🐠🐠🐠🐠🐠"
echo "🐠   INSTALL FISH   🐠"
echo "🐠🐠🐠🐠🐠🐠🐠🐠🐠🐠🐠"

sudo apt-get install fish -y
curl -L https://get.oh-my.fish | fish
omf install spacefish
## Make Fish default shell
sudo chsh -s /usr/bin/fish


sudo apt install fonts-firacode

## get dotfiles
echo
echo "--------------- DO .DOTFILES ---------------"
echo

cd ~

## add gitlab to know hosts
## not super secure. https://serverfault.com/questions/132970/can-i-automatically-add-a-new-host-to-known-hosts

## ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

if [ -d ~/dotfiles/ ]; then
    sudo rm -r ~/dotfiles -y
fi

git clone git@gitlab.com:smedegaard/dotfiles.git && echo "\n 🤖🤖🤖 CLONED \n" || echo "\n🤖🤖🤖 NOT CLONED \n"


mv ~/dotfiles ~/.dotfiles

if [ -d ~/.emacs.d/ ]; then
    mkdir ~/emacs-backup
    mv ~/.emacs.d/init.el ~/emacs-backup/init.el
fi

if [ -f ~/.spacemacs ]; then
    mv ~/.spacemacs ~/.spacemacs.old
fi

## make symbolic links to the dotfiles
echo "🔗 🔗 🔗 🔗 🔗 🔗 🔗 🔗 🔗 🔗 🔗"
echo "         SYM THE LINKS          "
echo "🔗 🔗 🔗 🔗 🔗 🔗 🔗 🔗 🔗 🔗 🔗"

ln -sv ~/.dotfiles/.emacs.d/init.el ~/.emacs.d/init.el
ln -sv ~/.dotfiles/.spacemacs ~/.spacemacs
ln -sv ~/.dotfiles/config.fish ~/.config/fish/config.fish
ln -sv ~/.dotfiles/fishfile ~/.config/fish/fishfile

echo
echo "install SNAPS" 
echo

sudo snap install spotify
sudo snap install slack --classic
sudo snap install thunderbird

echo
echo "download Spacemacs Icon"
echo

git clone https://github.com/nashamri/spacemacs-logo.git && echo " CLONED" || echo "COULD NOT GET SPACEMACS ICON"

mv ~/spacemacs-logo/ ~/.icons

echo "----------------------------------------------------------"
echo "| CHANGE 'Icon' in /usr/share/applications/emacs.desktop |"
echo "----------------------------------------------------------"



echo
echo "----- DONE 👍 -----"
echo
